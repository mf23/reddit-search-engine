import UIKit

extension UIView {
    func fillConstraintsToSuperview(_ multiplier: CGFloat = 1) {
        guard let superview = self.superview else { return }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        superview.addConstraint(NSLayoutConstraint(
            item: superview,
            attribute: .width,
            relatedBy: .equal,
            toItem: self,
            attribute: .width,
            multiplier: 1,
            constant: 0.0)
        )
        
        superview.addConstraint(NSLayoutConstraint(
            item: superview,
            attribute: .height,
            relatedBy: .equal,
            toItem: self,
            attribute: .height,
            multiplier: 1,
            constant: 0.0)
        )
        
        superview.addConstraint(NSLayoutConstraint(
            item: superview,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: self,
            attribute: .centerX,
            multiplier: 1,
            constant: 0.0)
        )
        
        superview.addConstraint(NSLayoutConstraint(
            item: superview,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: self,
            attribute: .centerY,
            multiplier: 1,
            constant: 0.0)
        )
    }

}
