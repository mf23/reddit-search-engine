import UIKit
import Kingfisher
import RealmSwift

final class GalleryCollectionViewController: UICollectionViewController, GalleryCollectionViewLayoutDelegate {
    private let reuseIdentifier = "GalleryCell"
    lazy var model = try! Realm().objects(Item.self)
    private var notificationToken: NotificationToken?
    
    init() {
        super.init(collectionViewLayout: GalleryCollectionViewLayout())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let collectionView = self.collectionView else { return }
        
        if let patternImage = UIImage(named: "Pattern") {
            self.view.backgroundColor = UIColor(patternImage: patternImage)
        }
        
        collectionView.backgroundColor = .clear
        collectionView.register(GalleryCell.self, forCellWithReuseIdentifier: self.reuseIdentifier)
        
        guard let layout = collectionView.collectionViewLayout as? GalleryCollectionViewLayout else { return }
        layout.delegate = self
        
        self.notificationToken = self.model.observe { [weak self] changes in
            switch changes {
            case .update(_, deletions: _, insertions: _, modifications: _):
                self?.collectionView?.reloadData()
            default: break
            }
        }
    }
    
    deinit {
        self.notificationToken?.invalidate()
    }
    
    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.model.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GalleryCell
        cell.imageURL = self.model[indexPath.item].url
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = FullScreenItemViewController(item: self.model[indexPath.item])
        self.show(controller, sender: nil)
    }
    
    //MARK: GalleryCollectionViewLayoutDelegate
    func heightImage(_ collectionView: UICollectionView, indexPath: IndexPath) -> CGFloat {
        return CGFloat(self.model[indexPath.item].imageHeight)
    }
    
    func widthImage(_ collectionView: UICollectionView, indexPath: IndexPath) -> CGFloat {
        return CGFloat(self.model[indexPath.item].imageWidth)
    }
}

final class GalleryCell: UICollectionViewCell {
    private let imageView = UIImageView()
    
    var imageURL: URL? {
        didSet {
            if let url = self.imageURL {
                KingfisherManager.shared.retrieveImage(with: url, options: .none, progressBlock: nil) { image, _, _, _ in
                    DispatchQueue.main.async {
                        self.imageView.image = image
                    }
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        self.addSubview(self.imageView)
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        self.imageView.fillConstraintsToSuperview()
    }
}
