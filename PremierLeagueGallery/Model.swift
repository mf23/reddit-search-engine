import Foundation
import RealmSwift

final class Item: Object {
    @objc dynamic var identifier = ""
    @objc dynamic var author = ""
    @objc dynamic var urlRaw = ""
    @objc dynamic var imageWidth: Float = 0
    @objc dynamic var imageHeight: Float = 0
    @objc dynamic var title = ""

    override static func primaryKey() -> String? {
        return "identifier"
    }
    
    public var url: URL {
        set { self.urlRaw = newValue.absoluteString }
        get { return URL(string: self.urlRaw)! }
    }
    
    override public static func ignoredProperties() -> [String] {
        return ["contentType"]
    }
}
