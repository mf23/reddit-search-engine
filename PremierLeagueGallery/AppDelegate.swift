import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let controller: UIViewController
        if try! Realm().objects(Item.self).isEmpty {
            controller = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()!
        } else {
            controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "galleryNavigation")
        }
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window!.rootViewController = controller
        self.window!.makeKeyAndVisible()
        
        return true
    }
    
    func restart(_ animationOption: UIViewAnimationOptions) {
        let controllerReward = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        self.window!.rootViewController = controllerReward
        UIView.transition(
            with: self.window!,
            duration: 0.5,
            options: animationOption,
            animations: {
                let oldState = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                self.window!.rootViewController = controllerReward
                UIView.setAnimationsEnabled(oldState)
        },
            completion: {_ in
                
                let realm = try! Realm()
                try? realm.write {
                    realm.deleteAll()
                }
                Configurator.shared.empty()
        }
        )
    }
}

