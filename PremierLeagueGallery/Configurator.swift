import UIKit

final class Configurator: NSObject {
    static let shared = Configurator()
    
    var category: String? {
        get {
            return UserDefaults.standard.string(forKey: "RedditCategory")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "RedditCategory")
        }
    }
    
    func empty() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    }
}

