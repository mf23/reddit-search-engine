import UIKit
import Kingfisher

class FullScreenItemViewController: UIViewController {
    let item: Item
    private let imageView = UIImageView()
    private let authorLabel = UILabel()
    private let textLabel = UILabel()
    init(item: Item) {
        self.item = item
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .black
        self.view.addSubview(self.imageView)
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        let widthItem = CGFloat(self.item.imageWidth)
        let heightItem = CGFloat(self.item.imageHeight)
       
        
        var height = (heightItem / widthItem) * self.view.bounds.width
        var width = self.view.bounds.width
        if (heightItem > widthItem) && (height > self.view.bounds.height * 0.65) {
            height = self.view.bounds.height * 0.6
            width = (widthItem / heightItem) * height
        }
       
        self.view.addConstraint(NSLayoutConstraint(
            item: self.view,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: self.imageView,
            attribute: .centerX,
            multiplier: 1,
            constant: 0.0)
        )
        
        self.view.addConstraint(NSLayoutConstraint(
            item: self.view,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: self.imageView,
            attribute: .centerY,
            multiplier: 1,
            constant: 0.0)
        )
        
    self.imageView.addConstraint(NSLayoutConstraint(
            item: self.imageView,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1,
            constant: width)
        )
        
        self.imageView.addConstraint(NSLayoutConstraint(
            item: self.imageView,
            attribute: .height,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1,
            constant: height)
        )
        
        self.view.addSubview(self.textLabel)
        self.textLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraint(NSLayoutConstraint(
            item: self.textLabel,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: self.view,
            attribute: .centerX,
            multiplier: 1,
            constant: 0.0))
            
        self.view.addConstraint(NSLayoutConstraint(
                item: self.textLabel,
                attribute: .top,
                relatedBy: .equal,
                toItem: self.imageView,
                attribute: .bottom,
                multiplier: 1,
                constant: 8.0))
        
        self.view.addConstraint(NSLayoutConstraint(
        item: self.textLabel,
        attribute: .width,
        relatedBy: .equal,
        toItem: self.view,
        attribute: .width,
        multiplier: 1,
        constant: 0.0))
        
        self.textLabel.textColor = .white
        self.textLabel.text = self.item.title
        self.textLabel.numberOfLines = 0
        self.textLabel.textAlignment = .center
        self.textLabel.font = self.textLabel.font.withSize(self.view.bounds.height / 40.0)

        self.authorLabel.text = NSLocalizedString("AUTHOR", comment: "Author Key") +  ": \(self.item.author)"

        self.authorLabel.textAlignment = .left
        self.view.addSubview(self.authorLabel)
        self.authorLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraint(NSLayoutConstraint(
            item: self.authorLabel,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: self.view,
            attribute: .centerX,
            multiplier: 1,
            constant: 0.0))
        
        self.view.addConstraint(NSLayoutConstraint(
            item: self.authorLabel,
            attribute: .bottom,
            relatedBy: .equal,
            toItem: self.imageView,
            attribute: .top,
            multiplier: 1,
            constant: -8.0))
        
        self.view.addConstraint(NSLayoutConstraint(
            item: self.authorLabel,
            attribute: .width,
            relatedBy: .equal,
            toItem: self.view,
            attribute: .width,
            multiplier: 1,
            constant: 0.0))
        
        self.authorLabel.textColor = .white
        self.authorLabel.font = UIFont.boldSystemFont(ofSize: self.view.bounds.height / 32.0)
        self.authorLabel.textAlignment = .left

        KingfisherManager.shared.retrieveImage(with: self.item.url, options: .none, progressBlock: nil) { image, _, _, _ in
                self.imageView.image = image
        }
    }
}
