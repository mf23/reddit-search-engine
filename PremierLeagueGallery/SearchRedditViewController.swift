import UIKit
import RealmSwift

final class SearchRedditViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var welcomeLabel: UILabel!
    @IBOutlet var textfield: UITextField!
    @IBOutlet var buttonSearch: RoundedButton!
    private var spinnerIndicator: SpinnerIndicatorView?
    private var redditURL: URLComponents? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "reddit.com"
        URLSessionConfiguration.default.timeoutIntervalForRequest = 10
        return urlComponents
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let welcome = NSLocalizedString("WELCOME", comment: "search button")
        self.welcomeLabel.text = "\(welcome) Reddit Search Engine"
        self.buttonSearch.fillColor = UIColor(hex: 0xFF4501)
        self.buttonSearch.setTitle(NSLocalizedString("SEARCH", comment: "search button"), for: .normal)
        self.textfield.delegate = self
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTouched(_:)))
        self.view.addGestureRecognizer(gesture)
        self.textfield.placeholder = NSLocalizedString("TEXTFIELD_PLACEHOLDER", comment: "search button")
    }
    
    @objc func viewTouched(_ gesture: UIGestureRecognizer) {
        self.textfield.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.textfield.resignFirstResponder()
        return true
    }
    
    private func search() {
        guard var text = self.textfield.text, !text.isEmpty else {
            self.textfield.text = ""
            return
        }
        
        text = text.lowercased().components(separatedBy: .whitespaces).joined()
        guard var urlComponent = self.redditURL else {
            self.textfield.text = ""
            return
        }
        
        urlComponent.path = "/r/\(text)/top.json"
        guard let url = urlComponent.url else {
            self.textfield.text = ""
            return
        }
        
        self.spinnerIndicator = SpinnerIndicatorView(view: self.view)
        URLSession(configuration: URLSessionConfiguration.default).dataTask(with: url) { (data, response, error) in
            
            DispatchQueue.main.async {
                self.spinnerIndicator?.dismiss()
                self.spinnerIndicator = nil
            }
            
            guard let r = response, let httpresponse = r as? HTTPURLResponse, httpresponse.statusCode == 200 else {
                DispatchQueue.main.async {
                    self.present(self.alert(with: NSLocalizedString("MESSAGE_ERROR_JSON", comment: "json error decode")), animated: true, completion: nil)
                }
                return
            }
            
            guard error == nil else {
                DispatchQueue.main.async {
                    self.present(self.alert(with: error.debugDescription), animated: true, completion: nil)
                }
                return
            }

            guard let jsonData = data,
                let resources = try? JSONDecoder().decode(Reddit.self, from: jsonData) else {
                    DispatchQueue.main.async {
                       self.present(self.alert(with: NSLocalizedString("MESSAGE_ERROR_JSON", comment: "json error decode")), animated: true, completion: nil)
                    }
                    return
            }
            
            var items = [Item]()
            for child in resources.data.children {
                guard let preview = child.data.preview,
                    let image = preview.images.first
                    else { continue }
                
                let item = Item()
                item.identifier = child.data.id
                item.author = child.data.author
                item.title = child.data.title
                item.url = image.source.url
                item.imageWidth = Float(image.source.width)
                item.imageHeight = Float(image.source.height)
                items.append(item)
            }
            
            let realm = try! Realm()
            try? realm.write {
                realm.add(items, update: true)
            }
            
            Configurator.shared.category = text
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "galleryNavigation")
            
            DispatchQueue.main.async {
                self.present(controller, animated: true, completion: nil)
            }
            
            }.resume()
    }
    
    @IBAction func searchButtonTouched(_ sender: UIButton) {
        self.search()
    }
    
    func alert(with message: String) -> UIAlertController {
        let alert = UIAlertController(title:  NSLocalizedString("ERROR", comment: "json error decode"), message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in self.textfield.text = ""
        })
        
        return alert
    }
}

final class RoundedButton: UIButton {
    var withShadow: Bool = false
    var borderColor: UIColor = .white
    var fillColor: UIColor = .blue
    
    override func draw(_ rect: CGRect) {
        self.tintColor = .white
        self.layer.borderWidth = 3
        self.layer.backgroundColor = fillColor.cgColor
        self.layer.borderColor = self.borderColor.cgColor
        let corner = self.bounds.midY
        self.layer.cornerRadius = corner
        if self.withShadow {
            self.layer.shadowPath = CGPath(roundedRect: self.bounds, cornerWidth: corner, cornerHeight: corner, transform: nil)
            self.layer.shadowRadius = 0
            self.layer.shadowOffset = CGSize(width: 0, height: 3)
            self.layer.shadowOpacity = 0.2
            self.layer.masksToBounds = false
        }
    }
}

extension UIColor {
    convenience init(hex: Int, alpha: CGFloat = 1) {
        self.init(
            red: CGFloat(UInt8(hex >> 16 & 0xFF)) / 255,
            green: CGFloat(UInt8(hex >> 8 & 0xFF)) / 255,
            blue: CGFloat(UInt8(hex & 0xFF)) / 255,
            alpha: alpha
        )
    }
}
