import UIKit

class SpinnerIndicatorView: UIView {
    private lazy var container: UIView = {
        let container = UIView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        container.backgroundColor = UIColor.black
        container.alpha = 0.5
        container.center = self.center
        container.layer.cornerRadius = 5
        return container
    }()
    
    private lazy var spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinner.center = CGPoint(x: 35, y: 35)
        return spinner
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    convenience init(view: UIView) {
        self.init(frame: view.bounds)
        view.addSubview(self)
    }
    
    private func setup() {
        self.backgroundColor = UIColor.clear
        self.container.addSubview(self.spinner)
        self.addSubview(self.container)
        self.spinner.startAnimating()
    }
    
    func dismiss() -> Void {
        UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveEaseOut, animations: { self.alpha = 0 }) { finished in
            self.spinner.stopAnimating()
            self.removeFromSuperview()
        }
    }
}


