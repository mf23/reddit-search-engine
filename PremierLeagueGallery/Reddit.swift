import Foundation

struct Reddit: Decodable {
    let data: RedditData
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try! values.decode(RedditData.self, forKey: .data)
    }
}

struct RedditData: Decodable {
    let children: [Child]
    
    enum CodingKeys: String, CodingKey {
        case children
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.children = try! values.decode([Child].self, forKey: .children)
    }
}

struct Child: Decodable {
    let data: ChildData
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try! values.decode(ChildData.self, forKey: .data)
    }
}

struct ChildData: Decodable {
    let id: String
    let author: String
    let preview: Preview?
    let title: String
    
    enum CodingKeys: String, CodingKey {
        case id, author, preview, title
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try! values.decode(String.self, forKey: .id)
        self.author = try! values.decode(String.self, forKey: .author)
        self.title = try! values.decode(String.self, forKey: .title)
        self.preview = try? values.decode(Preview.self, forKey: .preview)
    }
}

struct Preview: Decodable {
    let images: [Image]
    
    enum CodingKeys: String, CodingKey {
        case images
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.images = try! values.decode([Image].self, forKey: .images)
    }
}

struct Image: Decodable {
    let source: Resolution
    
    enum CodingKeys: String, CodingKey {
        case source
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.source = try! values.decode(Resolution.self, forKey: .source)
    }
}

struct Resolution: Decodable {
    let url: URL
    let width: Int
    let height: Int
    
    enum CodingKeys: String, CodingKey {
        case url
        case width
        case height
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let urlString = try! values.decode(String.self, forKey: .url)
        self.url = URL(string: urlString)!
        self.width = try! values.decode(Int.self, forKey: .width)
        self.height = try! values.decode(Int.self, forKey: .height)
    }
}
