import UIKit
import RealmSwift

final class GalleryViewController: UIViewController, UISearchBarDelegate {
    @IBOutlet var containerView: UIView!
    @IBOutlet var searchBar: UISearchBar!
    private let galleryCollectionViewController = GalleryCollectionViewController()
    
    @objc func searchButton(_ button: UIButton) {
        (UIApplication.shared.delegate as? AppDelegate)?.restart(.transitionCurlUp)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = Configurator.shared.category
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("NEW_SEARCH", comment: "New Search"), style: .plain, target: self, action: #selector(self.searchButton(_:)))

        self.searchBar.delegate = self
        self.addChildViewController(self.galleryCollectionViewController)
        self.containerView.addSubview(self.galleryCollectionViewController.view)
        self.galleryCollectionViewController.view.fillConstraintsToSuperview()
        self.galleryCollectionViewController.didMove(toParentViewController: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.searchBar.resignFirstResponder()
    }
    
    // MARK: - UISearchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.becomeFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.search(searchText: searchBar.text)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.search(searchText: searchText)
    }
    
    private func search(searchText: String? = nil) {
        var items = try! Realm().objects(Item.self)
        
        if let text = searchText, !text.isEmpty {
            let namePredicate = NSPredicate(format: "title contains[c] %@", argumentArray: [text])
            items = items.filter(namePredicate)
        }
        self.galleryCollectionViewController.model = items
        self.galleryCollectionViewController.collectionView?.reloadData()
        self.galleryCollectionViewController.collectionViewLayout.invalidateLayout()
    }
}
